from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Client
from .forms import ClientForm, Users_Form

# Create your views here.

class Signup(generic.FormView):
    template_name = "home/signup.html"
    form_class = ClientForm
    success_url = reverse_lazy("home:login")

    def form_valid(self, form):
        client = form.save()
        return super(Signup, self).form_valid(form)


class Signup(generic.FormView):
	template_name = 'home/signup.html'
	form_class = ClientForm
	success_url = reverse_lazy('login')

	def form_valid(self, form):
		user = form.save()
		return super(Signup, self).form_valid(form)

class NewClient(generic.CreateView):
    template_name = "home/new_client.html"
    model = Client
    form_class = ClientForm
    url_success = reverse_lazy("home:list")


    def post(self, *args, **kwargs):
        p = Client()

        p.client = request.user
        p.level = request.POST["level"]
        p.status = request.POST["status"]

        p.save()



class Index(LoginRequiredMixin, generic.TemplateView):
    template_name = "home/index.html"
    login_url = '/login/'



class About(LoginRequiredMixin, generic.TemplateView):
    template_name = "home/about.html"
    login_url = "/login/"


class List_Client(LoginRequiredMixin, generic.ListView):
    template_name = "home/list.html"
    model = Client
