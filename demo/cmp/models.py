from django.db import models

from inv.models import BaseModel

# Create your models here.

class Provider(BaseModel):
    description = models.CharField(max_length=128, unique=True)
    address = models.CharField(max_length=256, null=True, blank=True)
    contact = models.CharField(max_length=128)
    phone = models.CharField(max_length=10, null=True, blank=True)
    email = models.CharField(max_length=64, null=True, blank=True)


    def __str__(self):
        return "{}".format(self.description)


    def save(self):
        self.description = self.description.upper()
        super(Provider, self).save()


    class  Meta:
        verbose_name_plural = "Providers"
