from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Provider
from .forms import NewProviderForm
# Create your views here.

##### CRUD PROVIDER #####

## Create
## generic.CreateView
class NewProvider(LoginRequiredMixin, generic.CreateView):
    template_name = "cmp/new_provider.html"
    model = Provider
    context_object_name = "obj"
    form_class = NewProviderForm
    login_url = "home:login"
    success_url = reverse_lazy("cmp:list_provider")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


## Retrieve
# generic.ListView
class ListProvider(LoginRequiredMixin, generic.ListView):
    template_name = "cmp/list_provider.html"
    queryset = Provider.objects.all()
    login_url = "home:login"
    context_object_name = "obj"

## Update

## Delete
