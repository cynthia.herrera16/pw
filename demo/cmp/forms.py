from django import forms

from .models import Provider


class NewProviderForm(forms.ModelForm):
    class Meta:
        model = Provider
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Provider Name",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
