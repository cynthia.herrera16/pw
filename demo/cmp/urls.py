from django.urls import path

from cmp import views

app_name = "cmp"

urlpatterns =[
    path('list_provider/', views.ListProvider.as_view(), name="list_provider"),
    path('new_provider/', views.NewProvider.as_view(), name="new_provider"),
]
