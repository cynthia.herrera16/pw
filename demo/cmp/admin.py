from django.contrib import admin

# Register your models here.

from .models import Provider

@admin.register(Provider)
class AdminProvider(admin.ModelAdmin):
    list_display = [
        "id",
        "description",
        "timestamp",
        "status",
        "contact",
        "email"
    ]
    list_display_links = ["description"]
    list_filter = ["status"]
    search_fields = ["description", "id"]
    # fieldsets = (
    #     (
    #         "Primary Info", {
    #             "fields": ("description",)
    #         }
    #     ),
    #         ("Raise", {
    #             "fields": ("address",)
    #         })
    # )
