from django import forms

from .models import Category, SubCategory, Brand, Measure, Product

class NewCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Category",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })





class NewSubCategoryForm(forms.ModelForm):
    class Meta:
        model = SubCategory
        fields = [
            "category",
            "description",
            "status",
        ]
        labels = {
            "description": "Category",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
            self.fields["category"].empty_label = "Choose Category"



class BrandForm(forms.ModelForm):
    class Meta:
        model = Brand
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Brand",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })


class MeasureForm(forms.ModelForm):
    class Meta:
        model = Measure
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Measure",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })




class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [ ## "__all__"
            "code",
            "bar_code",
            "description",
            "status",
            "price",
            "stock",
            "last_buy",
            "brand",
            "subcategory",
            "measure"
        ]
        exclude = ["user", "user_update", "timestamp", "update"]
        labels = {
            "code": "Code",
            "bar_code": "Bar Code",
            "description": "Product Description",
            "status": "Status",
            "price": "Price",
            "stock": "Stock",
            "last_buy": "Last Buy",
            "brand": "Brand",
            "subcategory": "SubCategory",
            "measure": "Measure",
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
