from django.shortcuts import render
from django.urls import reverse_lazy

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import Category, SubCategory, Brand, Measure, Product
from .forms import NewCategoryForm, NewSubCategoryForm, BrandForm, MeasureForm, ProductForm
# Create your views here.

##### CRUD Category #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListCategory(LoginRequiredMixin, generic.ListView):
    template_name = "inv/list_category.html"
    model = Category
    context_object_name = "obj"
    login_url = "home:login"

## generic.DetailView
class DetailCategory(LoginRequiredMixin, generic.DetailView):
    template_name = "inv/detail_category.html"
    model = Category
    login_url = "home:login"
    context_object_name = "obj"



##### CREATE #####
## generic.CreateView
class NewCategory(LoginRequiredMixin, generic.CreateView):
    template_name = "inv/new_category.html"
    model = Category
    context_object_name = "obj"
    form_class = NewCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_category")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateCategory(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/update_category.html"
    model = Category
    context_object_name = "obj"
    form_class = NewCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_category")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteCategory(LoginRequiredMixin, generic.DeleteView):
    template_name = "inv/delete_category.html"
    model = Category
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_category")




##### CRUD SUBCATEGORY #####

##### RETRIEVE #####
## generic.ListView
class ListSubCategory(LoginRequiredMixin, generic.ListView):
    template_name = "inv/list_subcategory.html"
    model = SubCategory
    context_object_name = "obj"
    login_url = "home:login"


class DetailSubCategory(LoginRequiredMixin, generic.DetailView):
    template_name = "inv/detail_subcategory.html"
    model = SubCategory
    login_url = "home:login"
    context_object_name = "obj"


##### CREATE #####
## generic.CreateView
class NewSubCategory(LoginRequiredMixin, generic.CreateView):
    template_name = "inv/new_subcategory.html"
    model = SubCategory
    context_object_name = "obj"
    form_class = NewSubCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_subcategory")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


##### UPDATE #####
## generic.UpdateView
class UpdateSubCategory(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/update_subcategory.html"
    model = SubCategory
    context_object_name = "obj"
    form_class = NewSubCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_subcategory")


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### DELETE #####
## generic.DeleteView
class DeleteSubCategory(LoginRequiredMixin, generic.DeleteView):
    template_name = "inv/delete_subcategory.html"
    model = SubCategory
    context_object_name = "obj"
    success_url = reverse_lazy("inv:list_subcategory")
    login_url = "home:login"


##### API's #####
## ws JSON

def wsList(request):
    data = serializers.serialize('json', Category.objects.filter(status=False))
    return HttpResponse(data, content_type="application/json")


##### CRUD BRAND #####

class ListBrand(LoginRequiredMixin, generic.ListView):
    template_name = "inv/brand_category.html"
    model = Brand
    context_object_name = "obj"
    login_url = "home:login"


class NewBrand(LoginRequiredMixin, generic.CreateView):
    template_name = "inv/new_brand.html"
    model = Brand
    form_class = BrandForm
    login_url = "home:login"
    context_object_name = "obj"
    success_url = reverse_lazy("inv:list_brand")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class UpdateBrand(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/update_brand.html"
    model = Brand
    form_class = BrandForm
    login_url = "home:login"
    context_object_name = "obj"
    success_url = reverse_lazy("inv:list_brand")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class DeleteBrand(LoginRequiredMixin, generic.DeleteView):
    template_name = "inv/delete_brand.html"
    model = Brand
    context_object_name = "obj"
    success_url = reverse_lazy("inv:list_brand")
    login_url = "home:login"



##### CRUD Measure #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListMeasure(LoginRequiredMixin, generic.ListView):
    template_name = "inv/list_measure.html"
    model = Measure
    context_object_name = "obj"
    login_url = "home:login"

## generic.DetailView
# class DetailCategory(LoginRequiredMixin, generic.DetailView):
#     template_name = "inv/detail_category.html"
#     model = Category
#     login_url = "home:login"
#     context_object_name = "obj"



##### CREATE #####
## generic.CreateView
class NewMeasure(LoginRequiredMixin, generic.CreateView):
    template_name = "inv/new_measure.html"
    model = Measure
    context_object_name = "obj"
    form_class = MeasureForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_measure")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateMeasure(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/update_measure.html"
    model = Measure
    context_object_name = "obj"
    form_class = MeasureForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_measure")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteMeasure(LoginRequiredMixin, generic.DeleteView):
    template_name = "inv/delete_measure.html"
    model = Measure
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_measure")


##### CRUD PRODUCT #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListProduct(LoginRequiredMixin, generic.ListView):
    template_name = "inv/list_product.html"
    model = Product
    context_object_name = "obj"
    login_url = "home:login"

## generic.DetailView
# class DetailCategory(LoginRequiredMixin, generic.DetailView):
#     template_name = "inv/detail_category.html"
#     model = Category
#     login_url = "home:login"
#     context_object_name = "obj"



##### CREATE #####
## generic.CreateView
class NewProduct(LoginRequiredMixin, generic.CreateView):
    template_name = "inv/new_product2.html"
    model = Product
    context_object_name = "obj"
    form_class = ProductForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_product")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateProduct(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/new_product2.html"
    model = Product
    context_object_name = "obj"
    form_class = ProductForm
    login_url = "home:login"
    success_url = reverse_lazy("inv:list_product")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
# class DeleteMeasure(LoginRequiredMixin, generic.DeleteView):
#     template_name = "inv/delete_measure.html"
#     model = Measure
#     context_object_name = "obj"
#     login_url = "home:login"
#     success_url = reverse_lazy("inv:list_measure")
