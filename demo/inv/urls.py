from django.urls import path

from inv import views

app_name = "inv"

urlpatterns = [
    path('list_category/', views.ListCategory.as_view(), name="list_category"),
    path('new_category/', views.NewCategory.as_view(), name="new_category"),
    path('update_category/<int:pk>/', views.UpdateCategory.as_view(), name="update_category"),
    path('delete_category/<int:pk>/', views.DeleteCategory.as_view(), name="delete_category"),
    path('new_subcategory/', views.NewSubCategory.as_view(), name="new_subcategory"),
    path('list_subcategory/', views.ListSubCategory.as_view(), name="list_subcategory"),
    path('update_subcategory/<int:pk>/', views.UpdateSubCategory.as_view(), name="update_subcategory"),
    path('delete_subcategory/<int:pk>/', views.DeleteSubCategory.as_view(), name="delete_subcategory"),
    path('detail_category/<int:pk>/', views.DetailCategory.as_view(), name="detail_category"),
    path('detail_subcategory/<int:pk>/', views.DetailSubCategory.as_view(), name="detail_subcategory"),
    path('ws/list_category/', views.wsList, name="ws_list_category"),
    path('list_brand/', views.ListBrand.as_view(), name="list_brand"),
    path('new_brand/', views.NewBrand.as_view(), name="new_brand"),
    path('update_brand/<int:pk>/', views.UpdateBrand.as_view(), name="update_brand"),
    path('delete_brand/<int:pk>/', views.DeleteBrand.as_view(), name="delete_brand"),
    path('list_measure/', views.ListMeasure.as_view(), name="list_measure"),
    path('new_measure/', views.NewMeasure.as_view(), name="new_measure"),
    path('update_measure/<int:pk>/', views.UpdateMeasure.as_view(), name="update_measure"),
    path('delete_measure/<int:pk>/', views.DeleteMeasure.as_view(), name="delete_measure"),
    path('list_product/', views.ListProduct.as_view(), name="list_product"),
    path('new_product/', views.NewProduct.as_view(), name="new_product"),
    path('update_product/<int:pk>/', views.UpdateProduct.as_view(), name="update_product"),
]
