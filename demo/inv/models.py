from django.db import models

from django.contrib.auth.models import User



# Create your models here.

class BrandQuerySet(models.QuerySet):
    def old(self):
        return self.filter(id__lt=3)


class BaseModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    user_update = models.IntegerField(blank=True, null=True)


    class Meta:
        abstract = True


class Category(BaseModel):
    description = models.CharField(
        max_length = 128,
        help_text = "Category Description",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Category, self).save()

    class Meta:
        verbose_name_plural = "Categories"



class SubCategory(BaseModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(
        max_length = 128,
        help_text = "SubCategory Description"
    )

    def __str__(self):
        return "{0} - {1}".format(self.category.description, self.description)


    def save(self):
        self.description = self.description.upper()
        super(SubCategory, self).save()

    class Meta:
        verbose_name_plural = "SubCategories"
        unique_together = ("category", "description")


class Brand(BaseModel):
    objects = BrandQuerySet.as_manager()
    description = models.CharField(
        max_length = 128,
        help_text = "Description Brand",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Brand, self).save()

    class Meta:
        verbose_name_plural = "Brands"



class Measure(BaseModel):
    description = models.CharField(
        max_length = 128,
        help_text = "Measure Description",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Measure, self).save()

    class Meta:
        verbose_name_plural = "Measure"


##### PRODUCT MODEL #####

class Product(BaseModel):
    code = models.CharField(max_length=16, unique=True)
    bar_code = models.CharField(max_length=16)
    description = models.CharField(max_length=128)
    price = models.FloatField(default=0)
    stock = models.IntegerField(default=0)
    last_buy = models.DateField(null=True, blank=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    measure = models.ForeignKey(Measure, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)


    def __str__(self):
        return "{}".format(self.description)


    def save(self):
        self.description = self.description.upper()
        super(Product, self).save()


    class Meta:
        verbose_name_plural = "Products"
        unique_together = ("code", "bar_code")
